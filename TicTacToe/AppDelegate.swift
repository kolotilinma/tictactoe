//
//  AppDelegate.swift
//  TicTacToe
//
//  Created by Михаил Колотилин on 24.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

