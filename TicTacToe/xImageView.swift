//
//  xImageView.swift
//  TicTacToe
//
//  Created by Михаил Колотилин on 25.03.2020.
//  Copyright © 2020 Михаил Колотилин. All rights reserved.
//

import UIKit

class xImageView: UIImageView {
    var player: String?
    var activated: Bool! = false
    
    func setPlayer(player: String) {
        self.player = player
        if activated == false {
            self.image = player == "x" ? #imageLiteral(resourceName: "Rectangle") : #imageLiteral(resourceName: "Oval")
            activated = true
        }
    }

}
